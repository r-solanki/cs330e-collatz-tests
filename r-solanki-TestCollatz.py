#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------
from pathlib import Path

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)

    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)

    # ----
    # eval
    # ----
    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_6(self):
        v = collatz_eval(472, 38)
        self.assertEqual(v, 144)

    def test_eval_7(self):
        v = collatz_eval(364, 670)
        self.assertEqual(v, 145)

    def test_eval_8(self):
        v = collatz_eval(704, 519)
        self.assertEqual(v, 171)

    def test_eval_9(self):
        v = collatz_eval(938, 754)
        self.assertEqual(v, 179)

    def test_eval_10(self):
        v = collatz_eval(624, 616)
        self.assertEqual(v, 132)

    def test_eval_11(self):
        v = collatz_eval(171, 491)
        self.assertEqual(v, 144)

    def test_eval_12(self):
        v = collatz_eval(644, 848)
        self.assertEqual(v, 171)

    def test_eval_13(self):
        v = collatz_eval(444, 249)
        self.assertEqual(v, 144)

    def test_eval_14(self):
        v = collatz_eval(397, 723)
        self.assertEqual(v, 171)

    def test_eval_15(self):
        v = collatz_eval(834, 687)
        self.assertEqual(v, 171)

    def test_eval_16(self):
        v = collatz_eval(483, 232)
        self.assertEqual(v, 144)

    def test_eval_17(self):
        v = collatz_eval(54, 286)
        self.assertEqual(v, 128)

    def test_eval_18(self):
        v = collatz_eval(66, 393)
        self.assertEqual(v, 144)

    def test_eval_19(self):
        v = collatz_eval(274, 864)
        self.assertEqual(v, 171)

    def test_eval_20(self):
        v = collatz_eval(421, 504)
        self.assertEqual(v, 142)

    def test_eval_21(self):
        v = collatz_eval(854, 803)
        self.assertEqual(v, 135)

    def test_eval_22(self):
        v = collatz_eval(731, 762)
        self.assertEqual(v, 140)

    def test_eval_23(self):
        v = collatz_eval(509, 557)
        self.assertEqual(v, 137)

    def test_eval_24(self):
        v = collatz_eval(89, 788)
        self.assertEqual(v, 171)

    def test_eval_25(self):
        v = collatz_eval(531, 626)
        self.assertEqual(v, 137)

    def test_eval_26(self):
        v = collatz_eval(297, 80)
        self.assertEqual(v, 128)

    def test_eval_27(self):
        v = collatz_eval(110, 508)
        self.assertEqual(v, 144)

    def test_eval_28(self):
        v = collatz_eval(620, 830)
        self.assertEqual(v, 171)

    def test_eval_29(self):
        v = collatz_eval(293, 954)
        self.assertEqual(v, 179)

    def test_eval_30(self):
        v = collatz_eval(150, 631)
        self.assertEqual(v, 144)

    def test_eval_31(self):
        v = collatz_eval(762, 395)
        self.assertEqual(v, 171)

    def test_eval_32(self):
        v = collatz_eval(776, 786)
        self.assertEqual(v, 122)

    def test_eval_33(self):
        v = collatz_eval(647, 435)
        self.assertEqual(v, 142)

    def test_eval_34(self):
        v = collatz_eval(151, 570)
        self.assertEqual(v, 144)

    def test_eval_35(self):
        v = collatz_eval(988, 88)
        self.assertEqual(v, 179)

    def test_eval_36(self):
        v = collatz_eval(795, 354)
        self.assertEqual(v, 171)

    def test_eval_37(self):
        v = collatz_eval(358, 169)
        self.assertEqual(v, 144)

    def test_eval_38(self):
        v = collatz_eval(502, 482)
        self.assertEqual(v, 142)

    def test_eval_39(self):
        v = collatz_eval(642, 901)
        self.assertEqual(v, 179)

    def test_eval_40(self):
        v = collatz_eval(562, 919)
        self.assertEqual(v, 179)

    def test_eval_41(self):
        v = collatz_eval(871, 240)
        self.assertEqual(v, 179)

    def test_eval_42(self):
        v = collatz_eval(305, 391)
        self.assertEqual(v, 144)

    def test_eval_43(self):
        v = collatz_eval(536, 64)
        self.assertEqual(v, 144)

    def test_eval_44(self):
        v = collatz_eval(527, 129)
        self.assertEqual(v, 144)

    def test_eval_45(self):
        v = collatz_eval(210, 832)
        self.assertEqual(v, 171)

    def test_eval_46(self):
        v = collatz_eval(351, 951)
        self.assertEqual(v, 179)

    def test_eval_47(self):
        v = collatz_eval(763, 258)
        self.assertEqual(v, 171)

    def test_eval_48(self):
        v = collatz_eval(116, 912)
        self.assertEqual(v, 179)

    def test_eval_49(self):
        v = collatz_eval(862, 370)
        self.assertEqual(v, 171)

    def test_eval_50(self):
        v = collatz_eval(723, 301)
        self.assertEqual(v, 171)

    def test_eval_51(self):
        v = collatz_eval(302, 192)
        self.assertEqual(v, 128)

    def test_eval_52(self):
        v = collatz_eval(642, 607)
        self.assertEqual(v, 132)

    def test_eval_53(self):
        v = collatz_eval(793, 331)
        self.assertEqual(v, 171)

    def test_eval_54(self):
        v = collatz_eval(648, 487)
        self.assertEqual(v, 142)

    def test_eval_55(self):
        v = collatz_eval(694, 597)
        self.assertEqual(v, 145)

    def test_eval_56(self):
        v = collatz_eval(4228, 5563)
        self.assertEqual(v, 215)

    def test_eval_57(self):
        v = collatz_eval(3859, 2346)
        self.assertEqual(v, 238)

    def test_eval_58(self):
        v = collatz_eval(2893, 1323)
        self.assertEqual(v, 209)

    def test_eval_59(self):
        v = collatz_eval(7845, 22)
        self.assertEqual(v, 262)

    def test_eval_60(self):
        v = collatz_eval(1544, 5551)
        self.assertEqual(v, 238)

    def test_eval_61(self):
        v = collatz_eval(3666, 9855)
        self.assertEqual(v, 262)

    def test_eval_62(self):
        v = collatz_eval(6090, 1396)
        self.assertEqual(v, 238)

    def test_eval_63(self):
        v = collatz_eval(3587, 3992)
        self.assertEqual(v, 238)

    def test_eval_64(self):
        v = collatz_eval(5397, 136)
        self.assertEqual(v, 238)

    def test_eval_65(self):
        v = collatz_eval(3580, 5224)
        self.assertEqual(v, 238)

    def test_eval_66(self):
        v = collatz_eval(906, 6545)
        self.assertEqual(v, 262)

    def test_eval_67(self):
        v = collatz_eval(3796, 605)
        self.assertEqual(v, 238)

    def test_eval_68(self):
        v = collatz_eval(8757, 8405)
        self.assertEqual(v, 203)

    def test_eval_69(self):
        v = collatz_eval(2448, 1345)
        self.assertEqual(v, 183)

    def test_eval_70(self):
        v = collatz_eval(4057, 6405)
        self.assertEqual(v, 262)

    def test_eval_71(self):
        v = collatz_eval(6680, 1158)
        self.assertEqual(v, 262)

    def test_eval_72(self):
        v = collatz_eval(6149, 8583)
        self.assertEqual(v, 262)

    def test_eval_73(self):
        v = collatz_eval(9692, 5942)
        self.assertEqual(v, 262)

    def test_eval_74(self):
        v = collatz_eval(4059, 7605)
        self.assertEqual(v, 262)

    def test_eval_75(self):
        v = collatz_eval(9776, 7721)
        self.assertEqual(v, 260)

    def test_eval_76(self):
        v = collatz_eval(1418, 9936)
        self.assertEqual(v, 262)

    def test_eval_77(self):
        v = collatz_eval(6431, 6253)
        self.assertEqual(v, 200)

    def test_eval_78(self):
        v = collatz_eval(2466, 8473)
        self.assertEqual(v, 262)

    def test_eval_79(self):
        v = collatz_eval(2534, 9774)
        self.assertEqual(v, 262)

    def test_eval_80(self):
        v = collatz_eval(8275, 7938)
        self.assertEqual(v, 252)

    def test_eval_81(self):
        v = collatz_eval(6889, 9575)
        self.assertEqual(v, 260)

    def test_eval_82(self):
        v = collatz_eval(5106, 6606)
        self.assertEqual(v, 262)

    def test_eval_83(self):
        v = collatz_eval(1241, 7100)
        self.assertEqual(v, 262)

    def test_eval_84(self):
        v = collatz_eval(8589, 3817)
        self.assertEqual(v, 262)

    def test_eval_85(self):
        v = collatz_eval(234, 5323)
        self.assertEqual(v, 238)

    def test_eval_86(self):
        v = collatz_eval(6190, 8774)
        self.assertEqual(v, 257)

    def test_eval_87(self):
        v = collatz_eval(7850, 670)
        self.assertEqual(v, 262)

    def test_eval_88(self):
        v = collatz_eval(1978, 1246)
        self.assertEqual(v, 180)

    def test_eval_89(self):
        v = collatz_eval(8351, 8799)
        self.assertEqual(v, 234)

    def test_eval_90(self):
        v = collatz_eval(3866, 8114)
        self.assertEqual(v, 262)

    def test_eval_91(self):
        v = collatz_eval(8369, 1796)
        self.assertEqual(v, 262)

    def test_eval_92(self):
        v = collatz_eval(5322, 4992)
        self.assertEqual(v, 192)

    def test_eval_93(self):
        v = collatz_eval(3914, 3668)
        self.assertEqual(v, 238)

    def test_eval_94(self):
        v = collatz_eval(7391, 6774)
        self.assertEqual(v, 257)

    def test_eval_95(self):
        v = collatz_eval(4644, 8316)
        self.assertEqual(v, 262)

    def test_eval_96(self):
        v = collatz_eval(2645, 7205)
        self.assertEqual(v, 262)

    def test_eval_97(self):
        v = collatz_eval(4539, 6354)
        self.assertEqual(v, 262)

    def test_eval_98(self):
        v = collatz_eval(5179, 537)
        self.assertEqual(v, 238)

    def test_eval_99(self):
        v = collatz_eval(5978, 4784)
        self.assertEqual(v, 236)

    def test_eval_100(self):
        v = collatz_eval(7726, 3674)
        self.assertEqual(v, 262)

    def test_eval_101(self):
        v = collatz_eval(4067, 4868)
        self.assertEqual(v, 215)

    def test_eval_102(self):
        v = collatz_eval(4768, 7922)
        self.assertEqual(v, 262)

    def test_eval_103(self):
        v = collatz_eval(5183, 1726)
        self.assertEqual(v, 238)

    def test_eval_104(self):
        v = collatz_eval(5736, 9291)
        self.assertEqual(v, 262)

    def test_eval_105(self):
        v = collatz_eval(5772, 1510)
        self.assertEqual(v, 238)

    def test_eval_106(self):
        v = collatz_eval(55286, 62986)
        self.assertEqual(v, 335)

    def test_eval_107(self):
        v = collatz_eval(42594, 80857)
        self.assertEqual(v, 351)

    def test_eval_108(self):
        v = collatz_eval(59513, 41077)
        self.assertEqual(v, 340)

    def test_eval_109(self):
        v = collatz_eval(14655, 96957)
        self.assertEqual(v, 351)

    def test_eval_110(self):
        v = collatz_eval(18595, 71807)
        self.assertEqual(v, 340)

    def test_eval_111(self):
        v = collatz_eval(61154, 47655)
        self.assertEqual(v, 340)

    def test_eval_112(self):
        v = collatz_eval(52770, 94980)
        self.assertEqual(v, 351)

    def test_eval_113(self):
        v = collatz_eval(41770, 77930)
        self.assertEqual(v, 351)

    def test_eval_114(self):
        v = collatz_eval(72304, 35489)
        self.assertEqual(v, 340)

    def test_eval_115(self):
        v = collatz_eval(80896, 45533)
        self.assertEqual(v, 351)

    def test_eval_116(self):
        v = collatz_eval(72942, 54770)
        self.assertEqual(v, 335)

    def test_eval_117(self):
        v = collatz_eval(58723, 7223)
        self.assertEqual(v, 340)

    def test_eval_118(self):
        v = collatz_eval(10554, 87726)
        self.assertEqual(v, 351)

    def test_eval_119(self):
        v = collatz_eval(81918, 59534)
        self.assertEqual(v, 351)

    def test_eval_120(self):
        v = collatz_eval(25688, 79875)
        self.assertEqual(v, 351)

    def test_eval_121(self):
        v = collatz_eval(54189, 77950)
        self.assertEqual(v, 351)

    def test_eval_122(self):
        v = collatz_eval(10346, 9699)
        self.assertEqual(v, 242)

    def test_eval_123(self):
        v = collatz_eval(46451, 55899)
        self.assertEqual(v, 340)

    def test_eval_124(self):
        v = collatz_eval(37193, 25161)
        self.assertEqual(v, 324)

    def test_eval_125(self):
        v = collatz_eval(67226, 21590)
        self.assertEqual(v, 340)

    def test_eval_126(self):
        v = collatz_eval(80743, 82000)
        self.assertEqual(v, 320)

    def test_eval_127(self):
        v = collatz_eval(12821, 53203)
        self.assertEqual(v, 340)

    def test_eval_128(self):
        v = collatz_eval(42702, 98644)
        self.assertEqual(v, 351)

    def test_eval_129(self):
        v = collatz_eval(28279, 65073)
        self.assertEqual(v, 340)

    def test_eval_130(self):
        v = collatz_eval(92290, 94285)
        self.assertEqual(v, 315)

    def test_eval_131(self):
        v = collatz_eval(63945, 29493)
        self.assertEqual(v, 340)

    def test_eval_132(self):
        v = collatz_eval(72495, 57441)
        self.assertEqual(v, 335)

    def test_eval_133(self):
        v = collatz_eval(56095, 2570)
        self.assertEqual(v, 340)

    def test_eval_134(self):
        v = collatz_eval(11331, 81659)
        self.assertEqual(v, 351)

    def test_eval_135(self):
        v = collatz_eval(2989, 10604)
        self.assertEqual(v, 262)

    def test_eval_136(self):
        v = collatz_eval(89983, 1241)
        self.assertEqual(v, 351)

    def test_eval_137(self):
        v = collatz_eval(30794, 25998)
        self.assertEqual(v, 308)

    def test_eval_138(self):
        v = collatz_eval(52127, 36918)
        self.assertEqual(v, 314)

    def test_eval_139(self):
        v = collatz_eval(81770, 18152)
        self.assertEqual(v, 351)

    def test_eval_140(self):
        v = collatz_eval(24524, 42860)
        self.assertEqual(v, 324)

    def test_eval_141(self):
        v = collatz_eval(60469, 98693)
        self.assertEqual(v, 351)

    def test_eval_142(self):
        v = collatz_eval(59681, 12126)
        self.assertEqual(v, 340)

    def test_eval_143(self):
        v = collatz_eval(53862, 32939)
        self.assertEqual(v, 340)

    def test_eval_144(self):
        v = collatz_eval(9216, 73118)
        self.assertEqual(v, 340)

    def test_eval_145(self):
        v = collatz_eval(18735, 18486)
        self.assertEqual(v, 261)

    def test_eval_146(self):
        v = collatz_eval(4504, 69261)
        self.assertEqual(v, 340)

    def test_eval_147(self):
        v = collatz_eval(7983, 87982)
        self.assertEqual(v, 351)

    def test_eval_148(self):
        v = collatz_eval(7617, 471)
        self.assertEqual(v, 262)

    def test_eval_149(self):
        v = collatz_eval(32132, 40964)
        self.assertEqual(v, 324)

    def test_eval_150(self):
        v = collatz_eval(87270, 43164)
        self.assertEqual(v, 351)

    def test_eval_151(self):
        v = collatz_eval(28409, 73810)
        self.assertEqual(v, 340)

    def test_eval_152(self):
        v = collatz_eval(9479, 86401)
        self.assertEqual(v, 351)

    def test_eval_153(self):
        v = collatz_eval(61858, 84698)
        self.assertEqual(v, 351)

    def test_eval_154(self):
        v = collatz_eval(18047, 73966)
        self.assertEqual(v, 340)

    def test_eval_155(self):
        v = collatz_eval(34912, 34353)
        self.assertEqual(v, 280)

    def test_eval_156(self):
        v = collatz_eval(449576, 986713)
        self.assertEqual(v, 525)

    def test_eval_157(self):
        v = collatz_eval(723182, 695728)
        self.assertEqual(v, 504)

    def test_eval_158(self):
        v = collatz_eval(820868, 371022)
        self.assertEqual(v, 509)

    def test_eval_159(self):
        v = collatz_eval(423665, 301349)
        self.assertEqual(v, 449)

    def test_eval_160(self):
        v = collatz_eval(852131, 807516)
        self.assertEqual(v, 525)

    def test_eval_161(self):
        v = collatz_eval(339600, 754848)
        self.assertEqual(v, 509)

    def test_eval_162(self):
        v = collatz_eval(274390, 393011)
        self.assertEqual(v, 441)

    def test_eval_163(self):
        v = collatz_eval(394025, 942734)
        self.assertEqual(v, 525)

    def test_eval_164(self):
        v = collatz_eval(270140, 801279)
        self.assertEqual(v, 509)

    def test_eval_165(self):
        v = collatz_eval(427707, 799823)
        self.assertEqual(v, 509)

    def test_eval_166(self):
        v = collatz_eval(917204, 600883)
        self.assertEqual(v, 525)

    def test_eval_167(self):
        v = collatz_eval(162936, 180941)
        self.assertEqual(v, 365)

    def test_eval_168(self):
        v = collatz_eval(358085, 493467)
        self.assertEqual(v, 449)

    def test_eval_169(self):
        v = collatz_eval(166886, 659532)
        self.assertEqual(v, 509)

    def test_eval_170(self):
        v = collatz_eval(26406, 881053)
        self.assertEqual(v, 525)

    def test_eval_171(self):
        v = collatz_eval(560077, 936339)
        self.assertEqual(v, 525)

    def test_eval_172(self):
        v = collatz_eval(30259, 416429)
        self.assertEqual(v, 449)

    def test_eval_173(self):
        v = collatz_eval(505016, 168788)
        self.assertEqual(v, 449)

    def test_eval_174(self):
        v = collatz_eval(93917, 724058)
        self.assertEqual(v, 509)

    def test_eval_175(self):
        v = collatz_eval(930919, 906843)
        self.assertEqual(v, 476)

    def test_eval_176(self):
        v = collatz_eval(414382, 238068)
        self.assertEqual(v, 449)

    def test_eval_177(self):
        v = collatz_eval(56850, 251655)
        self.assertEqual(v, 443)

    def test_eval_178(self):
        v = collatz_eval(437792, 181699)
        self.assertEqual(v, 449)

    def test_eval_179(self):
        v = collatz_eval(711261, 6845)
        self.assertEqual(v, 509)

    def test_eval_180(self):
        v = collatz_eval(878988, 217356)
        self.assertEqual(v, 525)

    def test_eval_181(self):
        v = collatz_eval(912416, 212892)
        self.assertEqual(v, 525)

    def test_eval_182(self):
        v = collatz_eval(848297, 339234)
        self.assertEqual(v, 525)

    def test_eval_183(self):
        v = collatz_eval(282150, 698220)
        self.assertEqual(v, 509)

    def test_eval_184(self):
        v = collatz_eval(496731, 159163)
        self.assertEqual(v, 449)

    def test_eval_185(self):
        v = collatz_eval(372729, 763357)
        self.assertEqual(v, 509)

    def test_eval_186(self):
        v = collatz_eval(422497, 197636)
        self.assertEqual(v, 449)

    def test_eval_187(self):
        v = collatz_eval(977052, 953881)
        self.assertEqual(v, 458)

    def test_eval_188(self):
        v = collatz_eval(518311, 304922)
        self.assertEqual(v, 470)

    def test_eval_189(self):
        v = collatz_eval(63204, 144210)
        self.assertEqual(v, 375)

    def test_eval_190(self):
        v = collatz_eval(488999, 48352)
        self.assertEqual(v, 449)

    def test_eval_191(self):
        v = collatz_eval(857816, 231212)
        self.assertEqual(v, 525)

    def test_eval_192(self):
        v = collatz_eval(75328, 805676)
        self.assertEqual(v, 509)

    def test_eval_193(self):
        v = collatz_eval(417382, 962938)
        self.assertEqual(v, 525)

    def test_eval_194(self):
        v = collatz_eval(501034, 486325)
        self.assertEqual(v, 426)

    def test_eval_195(self):
        v = collatz_eval(525475, 681152)
        self.assertEqual(v, 509)

    def test_eval_196(self):
        v = collatz_eval(294155, 913261)
        self.assertEqual(v, 525)

    def test_eval_197(self):
        v = collatz_eval(321898, 97851)
        self.assertEqual(v, 443)

    def test_eval_198(self):
        v = collatz_eval(636957, 270597)
        self.assertEqual(v, 509)

    def test_eval_199(self):
        v = collatz_eval(779478, 21920)
        self.assertEqual(v, 509)

    def test_eval_200(self):
        v = collatz_eval(225061, 228014)
        self.assertEqual(v, 368)

    def test_eval_201(self):
        v = collatz_eval(950294, 561970)
        self.assertEqual(v, 525)

    def test_eval_202(self):
        v = collatz_eval(199009, 131369)
        self.assertEqual(v, 383)

    def test_eval_203(self):
        v = collatz_eval(946697, 561778)
        self.assertEqual(v, 525)

    def test_eval_204(self):
        v = collatz_eval(855669, 627262)
        self.assertEqual(v, 525)

    def test_eval_205(self):
        v = collatz_eval(177841, 307015)
        self.assertEqual(v, 443)

    def test_eval_206(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)



    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1, 1, 1)
        self.assertEqual(w.getvalue(), "1 1 1\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        input_text = Path("RunCollatz.in").read_text()
        output_text = Path("RunCollatz.out").read_text()
        r = StringIO(input_text)
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), output_text)

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
